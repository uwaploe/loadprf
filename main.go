// Loadprf loads profile definitions into either the DPC or
// the DSC. This program is part of the Deep Profiler project.
package main

import (
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strings"

	dpcmsg "bitbucket.org/uwaploe/go-dpcmsg/v2"
	"github.com/gogo/protobuf/proto"
	"github.com/gomodule/redigo/redis"
	yaml "gopkg.in/yaml.v3"
)

var Version = "dev"
var BuildDate = "unknown"

var queues = map[string]struct {
	prfq string
	cmdq string
}{
	"dpc": {prfq: "mpc:profiles", cmdq: "mpc:commands"},
	"dsc": {prfq: "dpc:commands", cmdq: "dpc:commands"},
}

// For each Profile definition, load the appropriate "prf" and "pattern"
// commands into the DPC command queue on the DSC. These commands will be
// transmitted over the inductive modem link by the DPC monitor service.
//
// prfs := list of Profile definition structures
//
func dscLoad(prfs []dpcmsg.Profile, prfq, server string) error {
	conn, err := redis.Dial("tcp", server)
	if err != nil {
		return fmt.Errorf("Cannot access Redis server: %v", err)
	}
	defer conn.Close()

	lastpat := ""
	for _, prf := range prfs {
		msg, err := proto.Marshal(&prf)
		if err != nil {
			return fmt.Errorf("Encoding %q failed: %w", &prf, err)
		}
		pat := prf.GetPattern()
		// If we are done with the most recent pattern,
		// append the pattern name to the profile
		// queue.
		if pat != lastpat {
			if len(lastpat) > 0 {
				_, err = conn.Do("RPUSH", prfq,
					fmt.Sprintf("pattern %s", lastpat))
				if err != nil {
					return fmt.Errorf("Redis command (RPUSH) failed: %v", err)
				}
				log.Printf("pattern %s -> %q\n",
					lastpat,
					prfq)
			}
			lastpat = pat
		}

		_, err = conn.Do("RPUSH", prfq,
			fmt.Sprintf("prf %s",
				base64.StdEncoding.EncodeToString(msg)))
		if err != nil {
			return fmt.Errorf("Redis command (RPUSH) failed: %v", err)
		}

		log.Printf("%s -> %q\n", &prf, prfq)
	}

	if len(lastpat) > 0 {
		_, err = conn.Do("RPUSH", prfq,
			fmt.Sprintf("pattern %s", lastpat))
		if err != nil {
			return fmt.Errorf("Redis command (RPUSH) failed: %v", err)
		}
		log.Printf("pattern %s -> %q\n",
			lastpat,
			prfq)
	}

	return nil
}

// Load each Profile definition into the appropriate profile queue
// on the DPC.
func dpcLoad(prfs []dpcmsg.Profile, prfq, server string) error {
	conn, err := redis.Dial("tcp", server)
	if err != nil {
		return fmt.Errorf("Cannot access Redis server: %w", err)
	}
	defer conn.Close()

	var (
		qname string
		qcmd  string
	)
	lastpat := ""
	for _, prf := range prfs {
		pm, _, pat := prf.ToProfileMessage()
		b, err := json.Marshal(pm)
		if err != nil {
			return fmt.Errorf("JSON conversion failed: %v", err)
		}

		// If this profile is part of a pattern, store it
		// in a separate queue.
		if len(pat) > 0 {
			qname = fmt.Sprintf("pattern:%s", pat)
		} else {
			qname = prfq
		}

		// If we are done with the most recent pattern,
		// append the pattern-queue name to the profile
		// queue.
		if pat != lastpat {
			if len(lastpat) > 0 {
				_, err = conn.Do("RPUSH", prfq,
					fmt.Sprintf("pattern:%s", lastpat))
				if err != nil {
					return fmt.Errorf("Redis command (RPUSH) failed: %v", err)
				}
			}
			lastpat = pat
		}

		switch (&prf).GetQueueing() {
		case dpcmsg.Profile_REPLACE:
			conn.Send("DEL", qname)
			qcmd = "LPUSH"
		case dpcmsg.Profile_APPEND:
			qcmd = "RPUSH"
		case dpcmsg.Profile_PREPEND:
			qcmd = "LPUSH"
		}

		_, err = conn.Do(qcmd, qname, b)
		if err != nil {
			return fmt.Errorf("Redis command (%s) failed: %v", qcmd, err)
		}
		log.Printf("%s -> %q\n", &prf, qname)
	}

	if len(lastpat) > 0 {
		_, err = conn.Do("RPUSH", prfq,
			fmt.Sprintf("pattern:%s", lastpat))
		if err != nil {
			return fmt.Errorf("Redis command (RPUSH) failed: %v", err)
		}
	}

	return nil
}

func rdProfiles(r io.Reader) ([]dpcmsg.Profile, error) {
	prfs := make([]dpcmsg.Profile, 0)
	dec := yaml.NewDecoder(r)

	err := dec.Decode(&prfs)

	return prfs, err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] infile\n",
			os.Args[0])
		fmt.Fprintf(os.Stderr, "Load DP profile definitions from 'infile'\n\n")
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	server := flag.String("server", "localhost:6379",
		"network address (HOST:PORT) of Redis server")
	no_go := flag.Bool("no-go", false,
		"do not append a 'go' command to the message queue")
	device := flag.String("device", "",
		"Load the profiles into the Redis queue on the named device (dsc|dpc)")

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Build date: %s\n", BuildDate)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	f, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("open %q failed: %v", args[0], err)
	}

	prfs, err := rdProfiles(f)
	f.Close()
	if err != nil {
		log.Fatalf("Error reading profile file: %v", err)
	}

	log.Printf("Read %d profile definitions\n", len(prfs))

	b, err := json.Marshal(prfs)
	if err == nil {
		fmt.Println(string(b))
	}

	if len(*device) > 0 {
		systype := strings.ToLower(*device)
		qs, ok := queues[systype]
		if !ok {
			fmt.Fprintf(os.Stderr, "Unknown system type: %q\n", args[0])
			flag.Usage()
			os.Exit(1)
		}

		log.Printf("Loading profiles into %s", strings.ToUpper(systype))

		switch *device {
		case "dpc":
			err = dpcLoad(prfs, qs.prfq, *server)
		case "dsc":
			err = dscLoad(prfs, qs.prfq, *server)
		}

		if err != nil {
			log.Fatal(err)
		}

		if len(qs.cmdq) > 0 && !*no_go {
			conn, err := redis.Dial("tcp", *server)
			if err != nil {
				log.Fatalf("Cannot access Redis server: %v", err)
			}
			defer conn.Close()

			_, err = conn.Do("RPUSH", qs.cmdq, "go")
			if err != nil {
				log.Fatalf("Redis command (RPUSH) failed: %v", err)
			}

			if *device == "dpc" {
				n, _ := redis.Int(conn.Do("PUBLISH", "motor.control", "go"))
				if n == 0 {
					log.Printf("WARNING: no subscriber for \"go\" command")
				}
			}
		}
	}
}
