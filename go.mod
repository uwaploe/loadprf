module bitbucket.org/uwaploe/loadprf

go 1.20

require (
	bitbucket.org/uwaploe/go-dpcmsg/v2 v2.4.1
	github.com/alicebob/miniredis/v2 v2.30.2
	github.com/gogo/protobuf v1.3.2
	github.com/gomodule/redigo v1.8.9
	gopkg.in/yaml.v3 v3.0.1
)

require (
	bitbucket.org/uwaploe/go-dputil v1.5.2 // indirect
	bitbucket.org/uwaploe/go-mpc v1.3.1 // indirect
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/yuin/gopher-lua v1.1.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
