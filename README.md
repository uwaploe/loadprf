# DP Profile Loading

The *load_profiles* program reads a YAML format file defining one or more
profiles, creates profile descriptions, and (optionally) loads them into
the Deep Profiler Controller (DPC).

There are two FIFO queues on the DPC implemented by [Redis](http://redis.io)
lists; *mpc:profiles* contains the profile descriptions and *mpc:commands*
contains commands.

See the [Wiki](https://bitbucket.org/uwaploe/loadprf/wiki/Home) for a
detailed description of the profile specification format.

## Usage

``` shellsession
$ load_profiles --help
Usage: load_profiles [options] infile
Load DP profile definitions from 'infile'

  -device string
        Load the profiles into the Redis queue on the named device (dsc|dpc)
  -no-go
        do not append a 'go' command to the message queue
  -server string
        network address (HOST:PORT) of Redis server (default "localhost:6379")
  -version
        Show program version information and exit
```

The program can be run on the DPC or DSC (Docking Station Controller), set
the *-device* option appropriately. If no device is specified, the program
will write a [JSON](http://json.org) encoded version of the profile
descriptions suitable for sending to the DSC using the DP Network API.

![Flowchart](process.svg "load_profiles flow chart")
