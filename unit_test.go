package main

import (
	"encoding/base64"
	"strings"
	"testing"

	dpcmsg "bitbucket.org/uwaploe/go-dpcmsg/v2"
	miniredis "github.com/alicebob/miniredis/v2"
	"github.com/gomodule/redigo/redis"
	"google.golang.org/protobuf/proto"
)

var INPUT = `
- dir: DIVE
  depth: 2900
  maxtime: 12000
  exclude:
    - acs_1
- dir: RISE
  depth: 2600
  maxtime: 1500
  tstart: 3600
  requeue: 10
  queueing: REPLACE
  pattern: p1
- dir: DIVE
  depth: 2900
  maxtime: 1500
  requeue: 10
  pattern: p1
- dir: RISE
  depth: 300
  maxtime: 12000
- dir: DIVE
  depth: 2900
  maxtime: 12000
- dir: DOCKING
  queueing: APPEND
`

func helperReadProfs(t *testing.T) []dpcmsg.Profile {
	rdr := strings.NewReader(INPUT)
	profs, err := rdProfiles(rdr)
	if err != nil {
		t.Fatal(err)
	}

	return profs
}

func TestReadProfs(t *testing.T) {
	profs := helperReadProfs(t)
	if len(profs) != 6 {
		t.Errorf("Bad profile count; expected 6, got %d", len(profs))
	}

	// for _, p := range profs {
	// 	t.Logf("%+v", p)
	// }
}

func TestDpcLoad(t *testing.T) {
	rd := miniredis.RunT(t)
	profs := helperReadProfs(t)
	conn, err := redis.Dial("tcp", rd.Addr())
	if err != nil {
		t.Fatal(err)
	}

	if err := dpcLoad(profs, "mpc:profiles", rd.Addr()); err != nil {
		t.Fatal(err)
	}

	n, err := redis.Int(conn.Do("LLEN", "mpc:profiles"))
	if err != nil {
		t.Fatal(err)
	}
	expected := len(profs) - 1
	if n != expected {
		t.Errorf("Bad queue length; expected %d, got %d", expected, n)
	}

	n, err = redis.Int(conn.Do("LLEN", "pattern:p1"))
	if err != nil {
		t.Fatal(err)
	}
	expected = 2
	if n != expected {
		t.Errorf("Bad pattern queue length; expected %d, got %d", expected, n)
	}
}

func TestDscLoad(t *testing.T) {
	rd := miniredis.RunT(t)
	profs := helperReadProfs(t)
	conn, err := redis.Dial("tcp", rd.Addr())
	if err != nil {
		t.Fatal(err)
	}

	if err := dscLoad(profs, "dpc:commands", rd.Addr()); err != nil {
		t.Fatal(err)
	}
	n, err := redis.Int(conn.Do("LLEN", "dpc:commands"))
	if err != nil {
		t.Fatal(err)
	}
	expected := len(profs) + 1
	if n != expected {
		t.Errorf("Bad queue length; expected %d, got %d", expected, n)
	}

	val, err := redis.String(conn.Do("LINDEX", "dpc:commands", "3"))
	if err != nil {
		t.Fatal(err)
	}

	want := "pattern p1"
	if val != want {
		t.Errorf("Bad entry; expected %q, got %q", want, val)
	}

	val, err = redis.String(conn.Do("LINDEX", "dpc:commands", "0"))
	if err != nil {
		t.Fatal(err)
	}

	parts := strings.Split(val, " ")
	if len(parts) != 2 {
		t.Errorf("Invalid profile entry: %q", val)
	}

	decoded, err := base64.StdEncoding.DecodeString(parts[1])
	if err != nil {
		t.Errorf("Cannot decode entry: %v", err)
	}
	prf := dpcmsg.Profile{}
	if err := proto.Unmarshal(decoded, &prf); err != nil {
		t.Fatal(err)
	}

	if !proto.Equal(&prf, &profs[0]) {
		t.Errorf("Value mismatch; expected %q, got %q", &profs[0], &prf)
	}

}
